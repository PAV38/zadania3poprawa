import os
import requests
from bs4 import BeautifulSoup
from flask import Flask

url = {
    'login': 'https://e.sggw.waw.pl/login/index.php',
    'grades': 'http://e.sggw.waw.pl/grade/report/user/index.php?id=649'
}

username = 'PAV38'
password = 'Misiaczek-38'

payload = {
    'username': username,
    'password': password
}

app = Flask(__name__)
app.config.from_object(__name__)

@app.route('/')
def main():
    resultStr=''
    sessionObj = requests.session()
    sessionObj.post(url["login"], data=payload, verify=False)
    response = sessionObj.get(url["grades"], verify=False)
    html = response.text
    beautifulSoupObj = BeautifulSoup(html)
    evaluations = beautifulSoupObj.find('table', 'user-grade')
    findedImages = evaluations.find_all('img', title='Kategoria')
    categories = []
    for image in findedImages:
        tdTab = []
        tr = image.find_parent('td').find_parent('tr').find_next_siblings('tr')
        for trow in tr:
            td = trow.find_all('td', 'b1b')
            if len(td) <= 2:
                break
            tdTab.append((td[1].text, [td[0].find('a').text, td[0].find('a')['href']]))
        if len(tdTab) > 0:
            categories.append([image.find_parent('td').text, sorted(tdTab, reverse=True)])

    for category in categories:
        for category2 in category[1]:
            resultStr+= 'Nazwa: %s    Ocena: %s   Link:  %s' % (category2[1][0], category2[0], category2[1][1]) + '</br>'
        resultStr+= '</br>'
    return resultStr


@app.route('/zad2')
def main2():
    resultString = ''
    inputValue = 'Szymczaka 6'
    baseLink = 'http://maps.googleapis.com/maps/api/geocode/json'
    data = {'address': inputValue, 'sensor': 'false' }
    responseFromSite = requests.get(baseLink, params=data)
    firstValue = responseFromSite.json()['results'][0]['geometry']['location']['lat']
    secondValue = responseFromSite.json()['results'][0]['geometry']['location']['lng']
    keyID='6VHBcmAZPHdRfMQv3tn3'
    base2 = 'http://opencaching.pl/okapi/services/caches/search/nearest'
    data2 = { 'center': str(firstValue)+'|'+str(secondValue), 'consumer_key': keyID }
    secondResponse = requests.get(base2, params=data2)
    caches = '|'.join(secondResponse.json()['results'])
    baseLink = 'http://opencaching.pl/okapi/services/caches/geocaches'
    data = { 'cache_codes':caches , 'consumer_key': keyID }
    result = requests.get(baseLink, params=data)
    bigresult = result.json()
    skrzynki = []
    googleLink = 'http://maps.googleapis.com/maps/api/geocode/json'
    for box in bigresult:
        miniBox = bigresult[box]
        lat_lng = miniBox['location'].split('|')
        googledata = { 'latlng':lat_lng[0]+','+lat_lng[1] , 'sensor': 'false' }
        try:
            result2 = requests.get(googleLink, params=googledata)
            sortAddress =  (result2.json()['results'][0]['formatted_address'])
            boxAddress = [sortAddress, miniBox['name'], miniBox['status']]
            skrzynki.append(boxAddress)
        except:
            print "proba nieudana"
            continue

    skrzynki.sort(key=lambda x: x[2])

    for skrzynka in skrzynki:
        resultString+= skrzynka[0] + ' | ' + skrzynka[1] + ' | ' + skrzynka[2] + '</br>'
    return resultString


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int(os.getenv('PORT', 5000)), debug=True)